# TO DO :
- Show custom Marker on load (geoJson Data)
- Be able to edit geoJson Data loaded with leaflet image
- Fix hatching pattern on rectangle
- Generate image - CORS issue

# PLUGINS
- TO CUSTOM BUTTON IMG IN LEAFLET CONTROL: https://stackoverflow.com/questions/42072686/leaflet-create-custom-button-to-draw
- HATCHING RECTANGLE : https://github.com/teastman/Leaflet.pattern
- POLYLINE DECORATOR : https://github.com/bbecquet/Leaflet.PolylineDecorator
- IMAGE : https://github.com/rowanwins/leaflet-easyPrint