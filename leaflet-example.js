// import PolylineDecorator from "./PolylineDecorator";
// import polylineDecorator from "leaflet-polylinedecorator";
/* ---------- ON NEW FEATURE ---------- */

/* --------- CUSTOM MAP --------- */
// url = './plan.jpeg';
// var bounds = [[-26.5,-25], [1021.5,1023]];
// var image = L.imageOverlay(url, bounds);
//     map = new L.Map('map', {layers: [image], crs: L.CRS.Simple, minZoom : -1});
// map.fitBounds(bounds);
/* --------- MAP --------- */
var map = L.map('map')
		var tiles = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}.png', {
			maxZoom: 18,
			attribution: 'Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors',
    }).addTo(map);
/* ---------- ADD STRIPE PATTERN FROM LIBRARY ---------- */
var stripes = new L.StripePattern();
stripes.addTo(map);
/* --------- GET LATLNG ON CLICK --------- */
/* document.getElementById("map").addEventListener("click", () => {
  map.on('click', function(e) {
  // L.marker([e.latlng.lat, e.latlng.lng], {icon: redIcon}).addTo(map).bindPopup("I am a red leaf.");
  alert("Lat, Lon : " + e.latlng.lat + ", " + e.latlng.lng)

 });
}) */
var collection = {
  "type": "FeatureCollection",
  "features": [
    // {
    //   // feature for Mapbox DC
    //   'type': 'Feature',
    //   'geometry': {
    //     'type': 'Point',
    //     'coordinates': [
    //       -0.10711669921875001,
    //       51.50894278564982
    //     ]
    //   },
    //   'properties': {
    //     'icon': 'new redDot()' // à faire marcher
    //   }
    // },
    // {
    //   'type': 'Feature',
    //   'geometry': {
    //     'type': 'Polygon',
    //     'coordinates': [[
    //       [-0.1330375671386719, 51.50285245588863],
    //       [-0.0872039794921875, 51.51140005662908],
    //       [-0.0872039794921875, 51.51140005662908],
    //       [-0.11432647705078126, 51.491310651214135],
    //       [-0.11432647705078126, 51.491310651214135],
    //       [-0.1330375671386719, 51.50285245588863]
    //     ]]
    //   },
    //   'properties': {
    //   }
    // },
    {
      'type': 'Feature',
      'geometry': {
        'type': 'Polygon',
        'coordinates': [[
          [-0.12565612792968753, 51.50114274331631],
          [-0.12565612792968753, 51.514925474796925],
          [-0.08394241333007814, 51.514925474796925],
          [-0.08394241333007814, 51.50114274331631],
          [-0.12565612792968753, 51.50114274331631]
        ]]
      },
      'properties': {
        fillPattern: 'stripes'
      }
    }
  ]
};

/* --------- ON LOAD - GEOJSON --------- */
map.on('load', function() {
  
// L.geoJSON(collection.features).addTo(map); DIRECTEMENT
  var geoJsonLayer = L.geoJson(collection.features, { // OU EN AJOUTANT DU TRAITEMENT
    onEachFeature: function (feature, layer) {
        // layer.bindPopup('<h1>'+feature.properties.customproperty+'</h1>')
        if (feature.geometry.type === 'Polygon') {
            console.log(feature)
            if (feature.properties.fillPattern = 'stripes') {
              layer.setStyle({
                'fillOpacity': 0.4,
                'fillPattern': stripes
              })
            }

            /*
            layer.setStyle({
                'weight': 0,
                'fillOpacity': 0
            });
            var bounds = layer.getBounds();
            var center = bounds.getCenter();
            var marker = L.marker(center).addTo(map); */
        }
        if (feature.geometry.type === 'Point') {
          // const latlng = layer.getLatLng()
          // var marker = L.marker(latlng).addTo(map);
        }
    }
  }).addTo(map);
});
map.setView([51.505, -0.09], 13);
/* --------- GEOJSON TEST ON STATIC MARKER --------- */
/* var marker = new L.Marker([469.75, 370]);
var geojson = marker.toGeoJSON();
console.log(geojson); */

/* --------- CUSTOM MARKER ENABLE FOR LEAFLET-DRAW --------- */
var redDot= L.Icon.extend({
  options: {
      shadowUrl: null,
      iconAnchor: new L.Point(12, 12),
      iconSize: new L.Point(24, 24),
      iconUrl: 'leaf-red.png'
  }
});
var greenDot= L.Icon.extend({
  options: {
      shadowUrl: null,
      iconAnchor: new L.Point(12, 12),
      iconSize: new L.Point(24, 24),
      iconUrl: 'leaf-green.png'
  }
});

var LeafIcon = L.Icon.extend({
  options: {
      iconSize:     [20, 20],
      shadowSize:   [50, 64],
      iconAnchor:   [22, 94],
      shadowAnchor: [4, 62],
      popupAnchor:  [-3, -76]
  }
});
var greenIcon = new LeafIcon({iconUrl: 'leaf-green.png'}),
    redIcon = new LeafIcon({iconUrl: 'leaf-red.png'});
    // orangeIcon = new LeafIcon({iconUrl: 'leaf-orange.png'});

L.icon = function (options) {
    return new L.Icon(options);
};
/* --------- CUSTOM DESCRIPTION HOVER BUTTON - BUTTON SELECTED --------- */

L.drawLocal.draw.toolbar.buttons.polygon = 'Custom polygon description!';
L.drawLocal.draw.handlers.rectangle.tooltip.start = 'Not telling...';

/* --------- ENABLE LEAFLET-DRAW CUSTOM --------- */
var drawnItems = new L.FeatureGroup();
map.addLayer(drawnItems);

draw = {
  polyline: {
    metric: true,
    shapeOptions: {
      weight: 5,
      fillOpacity: 0,
    }
  },
  polygon: {
    allowIntersection: false,
    showArea: true,
    drawError: {
      color: '#b00b00',
      timeout: 1000
    },
    shapeOptions: {
      weight: 5,
      fillOpacity: 0
    }
  },
  rectangle: {
    shapeOptions: {
      weight: 5,
      fillOpacity: 0
    }
  },
  circle: {
    shapeOptions: {
      weight: 5,
      stroke: false,
      fillOpacity: 0.5
    }
  },
  marker: false
};
edit = {
  featureGroup: drawnItems,
  buffer: {
    replace_polylines: false,
    separate_buffer: true,
    buffer_style: {
      weight: 5,
      fillOpacity: 0,
      dashArray: '5, 20'
    }
  }
};
//Add the controls to the map
var drawControlSettings = {
  position: 'bottomleft',
  draw: draw,
  edit: edit
};
L.DrawToolbar.include({
  getModeHandlers: function (map) {
      return [
          {
              enabled: true,
              handler: new L.Draw.Marker(map, { icon: new redDot() }),
              title: 'Place red dot'
          },
          {
              enabled: true,
              handler: new L.Draw.Marker(map, { icon: new greenDot() }),
              title: 'Place green dot'
          },
          {
            enabled: true,
            handler: new L.Draw.Rectangle(map),
            title: 'Rectangle'
          },
          {
            enabled: true,
            handler: new L.Draw.Polygon(map),
            title: 'Polygon'
          },
          {
            enabled: true,
            handler: new L.Draw.Polyline(map), //(map, this.options.polyline)
            title: 'Ligne'// L.drawLocal.draw.toolbar.buttons.polyline
          },
          /* {
            enabled: true, //this.options.circle
            handler: new L.Draw.Circle(map),
            title: 'Cercle'
          } */
          
      ];
  }
});

var drawControl = new L.Control.Draw(drawControlSettings);
map.addControl(drawControl);

/* ---------- CUSTOM COLOR ON RECTANGLE ---------- */
drawControl.setDrawingOptions({
  rectangle: {
      shapeOptions: {
          color: 'red',
          opacity:0.05
      },
      fillPattern: stripes,
      stroke: false,
      fillOpacity: 0.8,
  },
  polyline: {
    shapeOptions: {
        color: '#0000FF'
    }
}
});



map.on(L.Draw.Event.CREATED, function (e) {
    // console.log(e)
    var type = e.layerType,
        layer = e.layer;
/* ---------- CUSTOM POP UP ON MARKER ---------- */
    if (type === 'marker') {
        layer.bindPopup('A popup!');
        // alert("Nouveau local Pop up")
    }
    /* --------- CUSTOM POLYLINE DECORATOR (ARROW) DYNAMIC --------- */
    if (type === 'polyline') {
      let arrowTest = e.layer
      L.polylineDecorator(arrowTest, {
        patterns: [
          {offset: '100%', repeat: 0, symbol: L.Symbol.arrowHead({pixelSize: 15, polygon: false, pathOptions: {stroke: true}})}
        ]
      }).addTo(map)
    }
    /* --------- RECTANGLE OUTSIDE STRIPED--------- */
    // if (type === 'rectangle') {
    //   let arrowTest = e.layer
    //   L.polylineDecorator(arrowTest, {
    //     // patterns: [
    //     //   {offset: 0, repeat: 20, fillColor: 'blue', symbol: L.Symbol.dash({pixelSize: 10})}
    //     // ]
    //   }).addTo(map)
    // .bindLabel('A sweet static label!', { noHide: true })
    // }
    /* --------- RECTANGLE INSIDE STRIPED--------- */
    if (type === 'rectangle') {
      // let arrowTest = e.layer
      var rectangle = new L.Rectangle(e.layer.getLatLngs(), {
        fillPattern: stripes,
        stroke: false,
        fillOpacity: 0.8,
      })
      .addTo(map)
      

    }
    drawnItems.addLayer(layer);
});

/* --------- CUSTOM CODE TO DELETE ARROW FROM DELETED POLYLINE --------- */
map.on(L.Draw.Event.DELETED, function (e) {
  const layer = e.layers._layers // 74 : {...}
  for (const key in layer) {
    if (layer.hasOwnProperty(key)) {
      const layerData = layer[key] // {...}
      if(layerData instanceof L.Polyline) {
        const layerDataLatLng = JSON.stringify(layerData.getLatLngs())
        // Find arrow related to deleted polyline
        map.eachLayer(function (layer) {
          if(layer instanceof L.Polyline) {
            const leftPolygoneMiddle = JSON.stringify(layer.getLatLngs()[1])
            if (layerDataLatLng.includes(leftPolygoneMiddle)) map.removeLayer(layer)
          }
        })
      }
    }
  }    
})

/* --------- GET LATLNG ON CLICK --------- */
 document.getElementById("save").addEventListener("click", () => {
    map.eachLayer(function (layer) {
        if( layer instanceof L.Marker || layer instanceof L.Rectangle || layer instanceof L.Polyline) {
          const geo = layer.toGeoJSON()
          if (layer instanceof L.Rectangle) {
            geo.properties = {
              fillPattern: 'stripes'
            }
          }
          collection.features.push(geo)
          console.log(geo)  
          // console.log(collection)  
        }
    });
}) 

/* --------- BUTTON ON MAP TO PRINT --------- */
L.easyPrint({
  title: 'My awesome print button',
  position: 'bottomright',
  sizeModes: ['A4Portrait', 'A4Landscape']
}).addTo(map);
// CORS issue same as: https://gis.stackexchange.com/questions/342533/setting-up-leaflet-easyprint-plugin
/* --------- BUTTON ON MAP TO DOWNLOAD --------- */
var printer = L.easyPrint({
  tileLayer: tiles,
  sizeModes: ['Current', 'A4Landscape', 'A4Portrait'],
  filename: 'myMap',
  exportOnly: true,
  hideControlContainer: true
}).addTo(map);
/* --------- CUSTOM BUTTON TO DOWNLOAD --------- */
function manualPrint () {
  printer.printMap('CurrentSize', 'MyManualPrint')
}
function generateImage () {
  leafletImage(map, function(err, canvas) {
    // now you have canvas
    // example thing to do with that canvas:
    var img = document.createElement('img');
    var dimensions = map.getSize();
    img.width = dimensions.x;
    img.height = dimensions.y;
    img.src = canvas.toDataURL();
    document.getElementById('images').innerHTML = '';
    document.getElementById('images').appendChild(img);
  });
}


// TO CUSTOM BUTTON IMG IN LEAFLET CONTROL: https://stackoverflow.com/questions/42072686/leaflet-create-custom-button-to-draw
// HATCHING RECTANGLE : https://github.com/teastman/Leaflet.pattern
// POLYLINE DECORATOR : https://github.com/bbecquet/Leaflet.PolylineDecorator
// IMAGE : https://github.com/rowanwins/leaflet-easyPrint